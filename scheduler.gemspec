Gem::Specification.new do |s|
  s.name        = 'scheduler'
  s.version     = '0.1'
  s.date        = '2010-04-28'
  s.summary     = "Meeting Scheduler"
  s.description = "Simple Console calendar"
  s.authors     = ["Sergey Plevako"]
  s.email       = 'splevako1@gmail.com'
  s.files       = ["lib/scheduler/scheduler.rb", "lib/scheduler/date_helper.rb", "lib/scheduler/request.rb"]
  s.homepage    =
    'http://rubygems.org/gems'
  s.license       = 'MIT'
end