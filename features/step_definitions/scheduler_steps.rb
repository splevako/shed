Given(/^the input$/) do |string|
  @input = string
end

When(/^I type$/) do |input|
  steps %{
    When I type #{input}
  }
end

When(/^I run Scheduler Application$/) do
  run `ruby -e "Scheduler.new"`
  puts all_stdout
end
