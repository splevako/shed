require 'aruba/cucumber'
require_relative '../../lib/scheduler/scheduler.rb'

Before do
  @aruba_timeout_seconds = 5
end

Before('@slow_process') do
  @aruba_io_wait_seconds = 5
end

