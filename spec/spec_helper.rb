# encoding: UTF-8

require 'rspec'
require_relative '../lib/scheduler/scheduler.rb'

RSpec.configure do |conf|

end

def capture_stdout(&block)
    original_stdout = $stdout
    $stdout = fake = StringIO.new
    begin
      yield
    ensure
      $stdout = original_stdout
    end
    fake.string
  end


