require 'spec_helper'

describe Scheduler do
  
  before do
    @app = Scheduler.new
  end
  
  #before do
  #  IO.any_instance.stub(:puts) # globally
  #  Scheduler.any_instance.stub(:puts) # or for just one class
  #end
  
  #before do
  #  $stdout.stub(:write) # and/or $stderr if needed
  #end
          
     it "Your processing system must process input as text" do
        STDIN.should_receive(:read).with("")  
     end
     
     it "The first line of the input text represents the company office hours, in 24 hour clock format" do
       first_line = "0900 1730"              
       @app.stub(:gets).and_return("")
       STDOUT.should_receive(:puts).with("")
     end
     
     it "The remainder of the input represents individual booking requests"
     it """Each booking request is in the following format:
           2011-03-17 10:17:06 EMP001
           2011-03-21 09:00 2
          """
   
   describe "Output" do
     it """Your system must provide a successful booking calendar as output, 
           with bookings being grouped chronologically by day""" do
           STDOUT.should_receive(:puts)
           $stdin.puts("0930 1730")
           
           output = capture_stdout { @app }
           output.should == "\n"
      end
   end
      
   describe "Constraints" do
     it "No part of a meeting may fall outside office hours"
     it "Meetings may not overlap"
     it "The booking submission system only allows one submission at a time"
     it "Bookings must be processed in the chronological order in which they were submitted"
     it "The ordering of booking submissions in the supplied input is not guaranteed"
   end
   
end