require_relative 'date_helper.rb'

#Represents basic schedule item object
class Request
  REQ_SEPARATOR = " "
  include DateHelper
  attr_accessor :submission_time, :employee_id, :start_time, :duration, :end_time
  
  def set_request_data(input)    
    @submission_time = get_date_from_string(input.split.first + REQ_SEPARATOR + input.split[1])
    @employee_id = input.split.last
  end
  
  def set_meeting_data(input)
    @start_time = get_date_from_string(input.split.first + REQ_SEPARATOR + input.split[1])
    @duration = input.split.last.to_i
    @end_time = add_hours(@start_time, @duration)
  end
  
end