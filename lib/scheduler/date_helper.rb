require 'date'

#Basic time manipulations
module DateHelper
  
  def get_time_from_string(string)
    time = DateTime.strptime(string, '%H%M')    
  end
  
  def get_date_from_string(string)   
    DateTime.parse(string)
  end
  
  # date must be DateTime instance
  def add_hours(date, hours)
    raise "#{date} Must be DateTime" unless date.class == DateTime
    date + Rational(hours.to_i, 24)
  end
  
  def get_short_date(date)
    date.strftime('%Y-%M-%d')
  end
  
  def get_short_time(date)
    date.strftime('%H:%M')
  end
  
end