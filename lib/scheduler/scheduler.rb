require_relative 'date_helper.rb'
require_relative 'request.rb'
class Scheduler
  include DateHelper
  
  attr_reader :hours_from, :hours_to
  attr_accessor :items, :request
  
  def initialize(*args)
    #Request's holder  
    @items = Array.new
  
    $stdin.each_with_index do |line, index|
        line = line.chomp        
        unless line.nil? or line.empty?
          if index == 0
            get_working_hours(line)    
          else
            begin                                     
              unless (index % 2).zero?
                @request = Request.new 
                @request.set_request_data(line)                 
              else                
                @request.set_meeting_data(line)
                if request_is_working_hours? and request_no_overlap?                  
                  @items << @request
                end               
              end                                      
            end
          end
        end
        break if line == "\n" or line.empty?                 
    end#loop
    output
  end
  
  #TODO: No part of a meeting may fall outside office hours
  def request_is_working_hours?
     (@request.start_time.hour >= @hours_from.hour) and
     (@request.start_time.min >= @hours_from.min) and
     (@request.end_time.hour <= @hours_to.hour) and
     (@request.end_time.min <= @hours_to.min) 
  end
  
  #TODO: Meetings may not overlap
  def request_no_overlap?
    result = true
    @items.each { |item|
      #Proceed if only days are equal
      if item.start_time.day == @request.start_time.day
        if item.start_time.to_time <= @request.start_time.to_time and
           item.end_time.to_time >= @request.end_time.to_time
           result = false
        #TODO: Remove overlapping item if it was added before   
        #elsif (item.submission_time.to_time <  @request.submission_time.to_time)
        #   @items.delete(item)
        #   result = true 
        end                                       
      end    
    }
    result
  end

  #Get working hours from the very first input sting
  def get_working_hours(line)           
    @hours_from = get_time_from_string(line.split.first)
    @hours_to = get_time_from_string(line.split.last)    
  end
    
  #Print the output
  def output            
    @items.each { |item|
      puts get_short_date(item.start_time)
      puts "#{get_short_time(item.start_time)} #{get_short_time(item.end_time)} #{item.employee_id}"      
    }    
  end

end
